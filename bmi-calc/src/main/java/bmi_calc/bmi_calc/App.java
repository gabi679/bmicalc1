package bmi_calc.bmi_calc;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * App.java
 * Version 1.0
 * Made by: Harangozó Gábor
 * 2017.10.30
 */


public class App 
{

    public static double getHeightinCm(){
        double height=0;// the people height's
        while(true){
            System.out.println( "Your height in cm: " );
            try{
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                 height = Double.parseDouble(bufferRead.readLine());
                if(height<50.0){
                    System.out.println("It's seems too small, please give me your height in CM!");
                   }else if(height>=250.0){
                       System.out.println("It's seems too high, are you sure that you are more than 250cm? Please give me your real height in CM");
                   }else {
                       break;
                }
            
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            catch(NumberFormatException e){
                System.out.println("Your height can't be a text, please give me a number.");
            }

        }
        return height;
    }
    public static double getHeightinIn(){
        double height=0;// the people height's
        while(true){
            System.out.println( "Your height in Inch: " );
            try{
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                 height = Double.parseDouble(bufferRead.readLine());
                if(height<20.0){
                    System.out.println("It's seems too small, please give me your height in INCH!");
                   }else if(height>=100.0){
                       System.out.println("It's seems too high, are you sure that you are more than 100inch? Please give me your real height in INCH");
                   }else {
                       break;
                }
            
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            catch(NumberFormatException e){
                System.out.println("Your height can't be a text, please give me a number.");
            }

        }
        return height*2.54;//give back the data in cm
    }
    public static double getWeightinKg(){
        double weight=0;// the people weight's
        while(true){
            System.out.println( "Your weight in kg: " );
            try{
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                 weight = Double.parseDouble(bufferRead.readLine());
                if(weight<5.0){
                    System.out.println("You seems too easy, are you sure that you are less than 5kg?");
                   }else if(weight>=450.0){
                       System.out.println("You seems too heavy, are you sure that you are more than 450kg? Please give me your real weight in KG");
                   }else {
                       break;
                }
            
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            catch(NumberFormatException e){
                System.out.println("Your weight can't be a text, please give me a number.");
            }

        }
        return weight;
    }

  
    public static double getWeightinLbs(){
        double weight=0;// the people weight's
        while(true){
            System.out.println( "Your weight in lbs: " );
            try{
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                 weight = Double.parseDouble(bufferRead.readLine());
                if(weight<11.0){
                    System.out.println("You seems too easy, are you sure that you are less than 11lbs? Please give me your real weight in LBS");
                   }else if(weight>=990.0){
                       System.out.println("You seems too heavy, are you sure that you are more than 990lbs? Please give me your real weight in LBS");
                   }else {
                       break;
                }
            
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            catch(NumberFormatException e){
                System.out.println("Your weight can't be a text, please give me a number.");
            }

        }
        return weight/2.205;
    }
  

  public static void Main(String[] args)
  {
    Double height = Double.valueOf(0.0);
    Double weight = Double.valueOf(0.0);
    
      System.out.println("How would you want to give me your height?(cm/inch)");
      BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
      try {while(true){
        String answer = bufferRead.readLine();
        if (answer.equals("cm")) {
          height = Double.valueOf(getHeightinCm());
          break; }
        if (answer.equals("inch")) {
          height = Double.valueOf(getHeightinIn());
          break;
        }else
        System.out.println("Please answer the question only with cm or inch! Thank you!");
      }
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    
      System.out.println("How would you want to give me your weight?(kg/lbs)");
      try {while(true){
        String answer = bufferRead.readLine();
        if (answer.equals("kg")) {
          weight =(getWeightinKg());
          break; }
        if (answer.equals("lbs")) {
          weight =(getWeightinLbs());
          break;
        }else
        System.out.println("Please answer the question only with kg or Lbs! Thank you!");
      }
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    
    
    BmiCalc calculator = new BmiCalc(weight, height);
    System.out.println(calculator.toString());
  }
}
