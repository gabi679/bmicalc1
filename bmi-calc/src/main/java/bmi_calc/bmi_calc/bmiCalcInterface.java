package bmi_calc.bmi_calc;

public abstract interface bmiCalcInterface
{
  public abstract double getBMI();
  
  public abstract String getCategory(double paramDouble);
}
