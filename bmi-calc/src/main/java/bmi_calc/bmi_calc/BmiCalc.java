package bmi_calc.bmi_calc;

public class BmiCalc implements bmiCalcInterface
{
  private double weight = 0.0;
  private double height = 0.0;
  
  public BmiCalc(double weight, double height) {
    this.height = height/100;
    this.weight = weight;
    
  }
  
  public String toString() { return "Your BMI: " + getBMI() + "\n You are: " + getCategory(getBMI()); }
  
  public double getBMI()
  {
    double BMI = 0.0;
    
    BMI = weight / (height * height);
    return BMI;
  }
  
  public String getCategory(double BMI) {
    if (BMI <= 16.0D)
      return "Severe Thinness";
    if (BMI <= 17.0D) {
      return "Moderate Thinness";
    }
    if (BMI <= 18.5D) {
      return "Mild Thinness";
    }
    if (BMI <= 25.0D) {
      return "Normal";
    }
    if (BMI <= 30.0D) {
      return "Overweight";
    }
    if (BMI <= 35.0D) {
      return "Obese Class I";
    }
    if (BMI <= 40.0D) {
      return "Obese Class II";
    }
    if (BMI > 40.0D)
      return "Obese Class III";
    return " ";
  }
}