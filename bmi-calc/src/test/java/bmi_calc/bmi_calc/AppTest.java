package bmi_calc.bmi_calc;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
public class AppTest {
private BmiCalc calculator;
@Before
public void setUp() throws Exception {
calculator = new BmiCalc(95,195);
}
@Test
public void testSouldCalculateBmiWhenBothValueIsOkay() {
    double result = calculator.getBMI();
    System.out.print(calculator.getBMI());
    assertEquals(24.983562,result, 0.001);
}
@Test
public void testSouldCalculateBmiCategoryWhenItIsSevereThinness() {
    String result = calculator.getCategory(10);
    assertEquals("Severe Thinness", result);
}
public void testSouldCalculateBmiCategoryWhenItIsModerateThinness() {
    String result = calculator.getCategory(16);
    assertEquals("Moderate Thinness", result);
}
public void testSouldCalculateBmiCategoryWhenItIsMildThinness() {
    String result = calculator.getCategory(18);
    assertEquals("Mild Thinness", result);
}
public void testSouldCalculateBmiCategoryWhenItIsNormal() {
    String result = calculator.getCategory(20);
    assertEquals("Normal", result);
}
public void testSouldCalculateBmiCategoryWhenItIsOverweight() {
    String result = calculator.getCategory(28);
    assertEquals("Overweight", result);
}
public void testSouldCalculateBmiCategoryWhenItIsObeseClassI() {
    String result = calculator.getCategory(32);
    assertEquals("Obese Class I", result);
}
public void testSouldCalculateBmiCategoryWhenItIsObeseClassII() {
    String result = calculator.getCategory(37);
    assertEquals("Obese Class II", result);
}
public void testSouldCalculateBmiCategoryWhenItIsObeseClassIII() {
    String result = calculator.getCategory(45);
    assertEquals("Obese Class III", result);
}

}
